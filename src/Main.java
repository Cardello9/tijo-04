class Main {
    private long getFactorial(final long digit) throws Exception {
        if(digit < 0) {
            System.err.println("Podano ujemna liczbe!");
            throw new Exception();
        } else if(digit > 20) {
            System.err.println("Podano zbyt duza liczbe!");
            throw new Exception();
        }
        if (digit < 1)
            return 1;
        else
            return digit * getFactorial(digit - 1);
        }
       public static void main(String[] args) {
           long digit = 13;
           Main main = new Main();
           try {
               long factorial = main.getFactorial(digit);
               System.out.printf("Silnia(%d) = %d \n", digit, factorial);
           } catch (Exception e) {
               System.err.println("Wystapil blad, nie mozna obliczyc silni!");
           }
    }
}
